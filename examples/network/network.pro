requires(qtHaveModule(network))

TEMPLATE      = subdirs
SUBDIRS       = \
                dnslookup \
                download \
                downloadmanager

qtHaveModule(widgets) {
    SUBDIRS +=  \
                blockingfortuneclient \
                broadcastreceiver \
                broadcastsender \
                fortuneclient \
                fortuneserver \
                http \
                loopback \
                threadedfortuneserver \
                googlesuggest \
                torrent \
                bearermonitor \
                multicastreceiver \
                multicastsender \
                tlsauthextensions

    # no QProcess
    !vxworks:!qnx:SUBDIRS += network-chat

    contains(QT_CONFIG, openssl):SUBDIRS += securesocketclient
    contains(QT_CONFIG, openssl-linked):SUBDIRS += securesocketclient
    contains(QT_CONFIG, openssl):SUBDIRS += tlsauthextensions
    contains(QT_CONFIG, openssl-linked):SUBDIRS += tlsauthextensions
}
