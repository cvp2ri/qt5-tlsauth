/****************************************************************************
**
** Copyright © 2013 Cable Television Laboratories, Inc.
** Contact: http://www.cablelabs.com
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtNetwork>
#include <QtCore>

#include "tlsauthserver.h"

TlsAuthServer::TlsAuthServer(QObject *parent)
    : QTcpServer(parent), serverStarted(false)
{
}

void TlsAuthServer::initiateServerAccept(int port, QString keyPath, QString certPath)
{
    this->keyPath = keyPath;
    this->certPath = certPath;
    listen(QHostAddress::Any, port);
    serverStarted = true;
    emit serverStateUpdated(serverStarted);
}

void TlsAuthServer::incomingConnection(qintptr socketDescriptor)
{
    socket = new QSslSocket;
    if (socket->setSocketDescriptor(socketDescriptor)) {
        socket->setPrivateKey(keyPath);
        socket->setLocalCertificate(certPath);

        connect(socket, SIGNAL(readyForTlsAuthClientExtension()), this, SLOT(setTlsAuthClientExtension()));
        connect(socket, SIGNAL(readyForTlsAuthServerExtension()), this, SLOT(setTlsAuthServerExtension()));
        connect(socket, SIGNAL(receiveTlsSupplementalData(const QSet<QTlsSupplementalDataEntry>&)), this, SLOT(tlsSupplementalDataReceived(const QSet<QTlsSupplementalDataEntry>&)));
        connect(socket, SIGNAL(readyForTlsAuthSupplementalData()), this, SLOT(setTlsAuthSupplementalData()));

        socket->startServerEncryption();
    }
    else
    {
        delete socket;
    }
}

void TlsAuthServer::setTlsAuthClientExtension()
{
    QByteArray array;

    quint8 authFormatType = 225; //TLSEXT_AUTHZDATAFORMAT_dtcp
    array.append(authFormatType);

    quint16 extensionType = 7; //TLSEXT_TYPE_client_authz
    QTlsExtension clientExtension(extensionType, array);

    socket->setTlsAuthClientExtension(clientExtension);
}

void TlsAuthServer::setTlsAuthServerExtension()
{
    QByteArray array;

    quint8 dtcpAuthFormatType = 225; //TLSEXT_AUTHZDATAFORMAT_dtcp
    array.append(dtcpAuthFormatType);

    quint8 auditProofAuthFormatType = 182; //TLSEXT_AUTHZDATAFORMAT_audit_proof
    array.append(auditProofAuthFormatType);

    quint16 extensionType = 8; //TLSEXT_TYPE_server_authz
    QTlsExtension serverExtension(extensionType, array);

    socket->setTlsAuthServerExtension(serverExtension);
}

void TlsAuthServer::tlsSupplementalDataReceived(const QSet<QTlsSupplementalDataEntry> &tlsSupplementalData)
{
    clientTlsSupplementalData = new QSet<QTlsSupplementalDataEntry>;
    *clientTlsSupplementalData = tlsSupplementalData;
    qDebug() << "server received supplemental data " << tlsSupplementalData;
}

void TlsAuthServer::setTlsAuthSupplementalData()
{
    QTlsExtension clientAuthExtension = socket->receivedTlsAuthClientExtension();
    QTlsExtension serverAuthExtension = socket->receivedTlsAuthServerExtension();
    qDebug() << "server received client auth extension " << clientAuthExtension;
    qDebug() << "server received server auth extension " << serverAuthExtension;

    QSet<QTlsSupplementalDataEntry> tlsAuthSupplementalData;

    quint8 entryType = 225; //TLSEXT_AUTHZDATAFORMAT_dtcp
    QByteArray data("server12345123451234512345");
    QTlsSupplementalDataEntry entry(entryType, data);
    tlsAuthSupplementalData.insert(entry);

    quint8 entryType2 = 182; //TLSEXT_AUTHZDATAFORMAT_audit_proof
    QByteArray data2("proof12345123451234512345");
    QTlsSupplementalDataEntry entry2(entryType2, data2);
    tlsAuthSupplementalData.insert(entry2);

    socket->setTlsAuthSupplementalData(tlsAuthSupplementalData);
}
