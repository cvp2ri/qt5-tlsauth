HEADERS   += \
    tlsauthwindow.h \
    tlsauthclient.h \
    tlsauthserver.h
SOURCES   += \
             main.cpp \
    tlsauthwindow.cpp \
    tlsauthclient.cpp \
    tlsauthserver.cpp
RESOURCES +=
FORMS     += \
    tlsauthwindow.ui
QT        += network widgets

# install
target.path = $$[QT_INSTALL_EXAMPLES]/network/tlsauthextensions
INSTALLS += target

OTHER_FILES += \
    Makefile

