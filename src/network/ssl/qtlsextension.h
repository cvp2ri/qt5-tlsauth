/****************************************************************************
**
** Copyright © 2013 Cable Television Laboratories, Inc.
** Contact: http://www.cablelabs.com
**
** This file is part of the QtNetwork module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QTLSEXTENSION_H
#define QTLSEXTENSION_H

#include <QtCore/qstring.h>
#include <QtNetwork/qssl.h>

QT_BEGIN_NAMESPACE


#ifndef QT_NO_SSL

class QTlsExtensionPrivate;
class Q_NETWORK_EXPORT QTlsExtension
{
public:
    //NOTE: extension_data length 0-2^16-1
    QTlsExtension();
    QTlsExtension(const quint16 &extensionType, const QByteArray &content);
    QTlsExtension(const QTlsExtension &other);
    ~QTlsExtension();
    QTlsExtension &operator=(const QTlsExtension &other);

    inline void swap(QTlsExtension &other)
    { qSwap(d, other.d); }

    bool operator==(const QTlsExtension &other) const;
    inline bool operator!=(const QTlsExtension &other) const { return !operator==(other); }

    quint16 extensionType() const;
    QByteArray content() const;

    friend Q_CORE_EXPORT uint qHash(const QTlsExtension &extension, uint seed = 0) Q_DECL_NOTHROW;
private:
    QScopedPointer<QTlsExtensionPrivate> d;
};

#ifndef QT_NO_DEBUG_STREAM
class QDebug;
Q_NETWORK_EXPORT QDebug operator<<(QDebug debug, const QTlsExtension &extension);
#endif

#endif // QT_NO_SSL

QT_END_NAMESPACE

#endif
