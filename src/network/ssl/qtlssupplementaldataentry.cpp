/****************************************************************************
**
** Copyright © 2013 Cable Television Laboratories, Inc.
** Contact: http://www.cablelabs.com
**
** This file is part of the QtNetwork module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

/*!
    \class QTlsSupplementalDataEntry
    \brief The QTlsSupplementalDataEntry class represents a supplemental
    data TLS handshake message.

    \reentrant
    \ingroup network
    \ingroup ssl
    \ingroup shared
    \inmodule QtNetwork

    QTlsSupplementalDataEntry provides access to the data provided in the
    supplemental data TLS handshake message.  A supplemental data handshake
    message may contain a number supplemental data entries.

    Supplemental data may be provided by the client, by the server or both.

    The server or client may choose to only send TLS extensions (and
    subsequently supplemental data) after the initial handshake has completed,
    ensuring TLS extension data and supplemental data is sent encrypted.

    The supplemental data content format is not specified here, as individual
    RFCs are responsible for registering a new entry type and defining the
    content format.

    The TLS supplemental data types are defined in an IANA-managed registry.  See:

    \list
    \li \l{http://tools.ietf.org/html/rfc4680#section-5}
    \li \l{http://www.iana.org/assignments/tls-parameters/tls-parameters.xml#tls-parameters-11}
    \endlist

*/

#include "qtlssupplementaldataentry.h"
#include "qtlssupplementaldataentry_p.h"

#ifndef QT_NO_DEBUG_STREAM
#include <QtCore/qdebug.h>
#endif

QT_BEGIN_NAMESPACE

/*!
    Constructs an empty QTlsSupplementalDataEntry object.
*/
QTlsSupplementalDataEntry::QTlsSupplementalDataEntry()
    : d(new QTlsSupplementalDataEntryPrivate)
{
}

/*!
    Constructs a QTlsSupplementalDataEntry object. The \a entryType
    and \a content are stored and made available via accessors.
*/
QTlsSupplementalDataEntry::QTlsSupplementalDataEntry(quint16 entryType, const QByteArray &content)
    : d(new QTlsSupplementalDataEntryPrivate)
{
    d->entryType = entryType;
    d->content = content;
}

/*!
    Constructs an identical copy of the \a other supplemental data entry.
*/
QTlsSupplementalDataEntry::QTlsSupplementalDataEntry(const QTlsSupplementalDataEntry &other)
    : d(new QTlsSupplementalDataEntryPrivate)
{
    *d.data() = *other.d.data();
}

/*!
    Destroys the QTlsSupplementalDataEntry object.
*/
QTlsSupplementalDataEntry::~QTlsSupplementalDataEntry()
{
}

/*!
    Copies the contents of \a other into this supplemental data entry, making the two
    supplemental data entries identical.
*/
QTlsSupplementalDataEntry &QTlsSupplementalDataEntry::operator=(const QTlsSupplementalDataEntry &other)
{
    *d.data() = *other.d.data();
    return *this;
}

/*!
    \fn void QTlsSupplementalDataEntry::swap(QTlsSupplementalDataEntry &other)
    \since 5.0

    Swaps this supplemental data entry instance with \a other. This function is very
    fast and never fails.
*/

/*!
    Returns true if this supplemental data entry is the same as \a other; otherwise,
    false is returned.
*/
bool QTlsSupplementalDataEntry::operator==(const QTlsSupplementalDataEntry &other) const
{
    return d->content == other.d->content && d->entryType == other.d->entryType;
}

/*!
    \fn bool QTlsSupplementalDataEntry::operator!=(const QTlsSupplementalDataEntry &other) const

    Returns true if this supplemental data entry is not the same as \a other;
    otherwise, false is returned.
*/

/*!
    Returns the entry type of the supplemental data entry.

    The entry type is a unique identifier representing the
    registered supplemental data type and can be used to
    determine the layout of data returned by the content function.

    \sa content()
*/
quint16 QTlsSupplementalDataEntry::entryType() const
{
    return d->entryType;
}

/*!
    Returns the content of the supplemental data entry.

    The format of the content returned in the QByteArray is specific to
    the entry type.

    \sa entryType()
*/
QByteArray QTlsSupplementalDataEntry::content() const
{
    return d->content;
}

/*!
    Returns the hash value for the \a entry. If specified, \a seed is used to
    initialize the hash.

    \relates QHash
*/
uint qHash(const QTlsSupplementalDataEntry &entry, uint seed) Q_DECL_NOTHROW
{
    if (!entry.d)
        return qHash(-1, seed); // the hash of an unset port (-1)

    return qHash(entry.d->content) ^
            qHash(entry.d->entryType);
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<(QDebug debug, const QTlsSupplementalDataEntry &supplementalDataEntry)
{
    QString hexValue;
    hexValue.setNum(supplementalDataEntry.entryType(), 16);
    debug << "QTlsSupplementalDataEntry(type: 0x"
          << hexValue
          <<", size:"
          << supplementalDataEntry.content().length()
          << ", first 8 bytes:"
          << supplementalDataEntry.content().left(8).toHex()
          << ", last 8 bytes:"
          << supplementalDataEntry.content().right(8).toHex()
          << ')';
    return debug;
}
#endif

QT_END_NAMESPACE
