/****************************************************************************
**
** Copyright © 2013 Cable Television Laboratories, Inc.
** Contact: http://www.cablelabs.com
**
** This file is part of the QtNetwork module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

/*!
    \class QTlsExtension
    \brief The QTlsExtension class represents a TLS extension.

    \reentrant
    \ingroup network
    \ingroup ssl
    \ingroup shared
    \inmodule QtNetwork

    QTlsExtension provides access to the data provided in the TLS
    extensions of a TLS hello message.

    The extension data content format is not specified here, as individual
    RFCs are responsible for registering a new entry type and defining the
    content format.  A hello message may contain a number of TLS extensions.

    A TLS extension of a specific type may only be sent by the server if the
    same extension type was received from the client.

    The server or client may choose to only send TLS extensions after the
    initial handshake has completed, ensuring TLS extension data is sent
    encrypted.

    NOTE: If the data exposed by content represents a collection of entries,
    the == operation on content may return false for sent and received
    extensions otherwise expected to be identical, as the order may change.
    Compare individual entries in the content collection instead.

    The TLS extension types are defined in an IANA-managed registry.  See:

    \list
    \li \l{http://tools.ietf.org/html/rfc5246#section-12}
    \li \l{http://tools.ietf.org/html/rfc6066}
    \li \l{http://www.iana.org/assignments/tls-extensiontype-values/tls-extensiontype-values.xml#tls-extensiontype-values-1}
    \endlist

*/

#include "qtlsextension.h"
#include "qtlsextension_p.h"

#ifndef QT_NO_DEBUG_STREAM
#include <QtCore/qdebug.h>
#endif

QT_BEGIN_NAMESPACE

/*!
    Constructs an empty QTlsExtension object.
*/
QTlsExtension::QTlsExtension()
    : d(new QTlsExtensionPrivate)
{
}

/*!
    Constructs a QTlsExtension object. The \a extensionType
    and \a content are stored and made available via accessors.
*/
QTlsExtension::QTlsExtension(const quint16 &extensionType, const QByteArray &content)
    : d(new QTlsExtensionPrivate)
{
    d->extensionType = extensionType;
    d->content = content;
}

/*!
    Constructs an identical copy of the \a other extension.
*/
QTlsExtension::QTlsExtension(const QTlsExtension &other)
    : d(new QTlsExtensionPrivate)
{
    *d.data() = *other.d.data();
}

/*!
    Destroys the QTlsExtension object.
*/
QTlsExtension::~QTlsExtension()
{
}

/*!
    Copies the contents of \a other into this extension, making the two
    extensions identical.
*/
QTlsExtension &QTlsExtension::operator=(const QTlsExtension &other)
{
    *d.data() = *other.d.data();
    return *this;
}

/*!
    \fn void QTlsExtension::swap(QTlsExtension &other)
    \since 5.0

    Swaps this extension instance with \a other. This function is very
    fast and never fails.
*/

/*!
    Returns true if this extension is the same as \a other; otherwise,
    false is returned.
*/
bool QTlsExtension::operator==(const QTlsExtension &other) const
{
    return d->content == other.d->content && d->extensionType == other.d->extensionType;
}

/*!
    \fn bool QTlsExtension::operator!=(const QTlsExtension &other) const

    Returns true if this extension is not the same as \a other;
    otherwise, false is returned.
*/

/*!
    Returns the extension type of the extension.

    The extension type is a unique identifier representing the
    registered extension type and can be used to
    determine the layout of data returned by the content function.

    \sa content()
*/
quint16 QTlsExtension::extensionType() const
{
    return d->extensionType;
}

/*!
    Returns the content of the extension.

    The format of the content returned in the QByteArray is specific to
    the extension type.

    \sa extensionType()
*/
QByteArray QTlsExtension::content() const
{
    return d->content;
}

/*!
    Returns the hash value for the \a entry. If specified, \a seed is used to
    initialize the hash.

    \relates QHash
*/
uint qHash(const QTlsExtension &extension, uint seed) Q_DECL_NOTHROW
{
    if (!extension.d)
        return qHash(-1, seed); // the hash of an unset port (-1)

    return qHash(extension.d->content) ^
            qHash(extension.d->extensionType);
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<(QDebug debug, const QTlsExtension &extension)
{
    debug << "QTlsExtension(type:"
          << extension.extensionType()
          << ", size:"
          << extension.content().length()
          << ", first 8 bytes:"
          << extension.content().left(8).toHex()
          << ", last 8 bytes:"
          << extension.content().right(8).toHex()
          << ')';
    return debug;
}
#endif

QT_END_NAMESPACE
